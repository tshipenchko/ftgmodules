import io, inspect, logging
from os import path
from .. import loader, utils

logger = logging.getLogger(__name__)

@loader.tds
class ModulesLinkMod(loader.Module):
    """Get link on module"""
    strings = {"name": "ModulesLink",
	           "repo_config_doc": "Fully qualified URL to a module repo",
	           "avail_header": "<b>Available official modules from repo</b>",
	           "select_preset": "<b>Please select a preset</b>",
	           "no_preset": "<b>Preset not found</b>",
	           "preset_loaded": "<b>Preset loaded</b>",
	           "no_module": "<b>Module not available in repo.</b>",
	           "no_file": "<b>File not found</b>",
	           "provide_module": "<b>Provide a module to load</b>",
	           "bad_unicode": "<b>Invalid Unicode formatting in module</b>",
	           "load_failed": "<b>Loading failed. See logs for details</b>",
	           "loaded": "<b>Module loaded.</b>",
	           "no_class": "<b>What class needs to be unloaded?</b>",
	           "unloaded": "<b>Module unloaded.</b>",
	           "not_unloaded": "<b>Module not unloaded.</b>",
	           "requirements_failed": "<b>Requirements installation failed</b>",
	           "requirements_installing": "<b>Installing requirements...</b>",
	           "requirements_restart": "<b>Requirements installed, but a restart is required</b>",
	           "all_modules_deleted": "<b>All modules deleted</b>",
	           "reply_to_txt": "<b>Reply to .txt file<b>",
	           "restored_modules": "<b>Loaded:</b> <code>{}</code>\n<b>Already loaded:</b> <code>{}</code>",
	           "backup_completed": "<b>Modules backup completed</b>\n<b>Count:</b> <code>{}</code>",
	           "no_modules": "<b>You have no custom modules!</b>",
	           "no_name_module": "<b>Type module name in arguments</b>",
	           "no_command_module": "<b>Type module command in arguments</b>",
	           "command_not_found": "<b>Command was not found!</b>",
	           "searching": "<b>Searching...</b>",
	           "file": "<b>File of module {}:<b>",
	           "module_link": "<a href=\"{}\">Link</a> for module {}: \n<code>{}</code>",
	           "not_found_info": "Request to find module with name {} failed due to:",
	           "not_found_c_info": "Request to find module with command {} failed due to:",
	           "not_found": "<b>Module was not found</b>",
	           "file_core": "<b>File of core module {}:</b>",
	           "loading": "<b>Loading...</b>",
	           "url_invalid": "<b>URL invalid</b>",
	           "args_incorrect": "<b>Args incorrect</b>",
	           "repo_loaded": "<b>Repository loaded</b>",
	           "repo_not_loaded": "<b>Repository not loaded</b>",
	           "repo_unloaded": "<b>Repository unloaded, but restart is required to unload repository modules</b>",
	           "repo_not_unloaded": "<b>Repository not unloaded</b>"}  # когда-нибудь я это уберу

    @loader.owner
	async def moduleinfocmd(self, message):
		"""Get link on module by one's command or name"""
		args = utils.get_args_raw(message).lower()
		if args.startswith(*self._db.get(__name__, "command_prefix", ["."])):
			args = args[1:]
			if not args: return await utils.answer(message, self.strings("no_command_module", message))
			if args in self.allmodules.commands.keys():
				args = self.allmodules.commands[args].__self__.strings["name"]
			elif args in self.allmodules.aliases.keys():
				args = self.allmodules.aliases[args]
				args = self.allmodules.commands[args].__self__.strings["name"]
			else:
				return await utils.answer(message, self.strings("command_not_found", message))
			message = await utils.answer(message, self.strings("searching", message))
			await self.send_module(message, args, False)
		else:
			args = utils.get_args_raw(message).lower()
			if not args: return await utils.answer(message, self.strings("no_name_module", message))
			message = await utils.answer(message, self.strings("searching", message))
			await self.send_module(message, args, True)

	async def send_module(self, message, args, by_name):
		"""Sends module by name"""
		try:
			f = ' '.join(
				[x.strings["name"] for x in self.allmodules.modules if
				 args.lower() == x.strings("name", message).lower()])
			r = inspect.getmodule(
				next(filter(lambda x: args.lower() == x.strings("name", message).lower(), self.allmodules.modules)))
			link = r.__spec__.origin

			core_module = False

			if link.startswith("http"):
				text = self.strings("module_link", message).format(link, f, link)
			elif link == "<string>":
				text = self.strings("file", message).format(f)
			elif path.isfile(link):
				core_module = True
				text = self.strings("file_core", message).format(f)
			else:
				text = self.strings("file", message).format(f)

			if core_module:
				with open(link, "rb") as file:
					out = io.BytesIO(file.read())
			else:
				out = io.BytesIO(r.__loader__.data)
			out.name = f + ".py"
			out.seek(0)

			await utils.answer(message, out, caption=text)
		except Exception as e:
			log_text = self.strings("not_found_info", message) if by_name else self.strings("not_found_info", message)
			logger.info(log_text.format(args), exc_info=True)
			await utils.answer(message, self.strings("not_found", message))