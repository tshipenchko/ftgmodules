# https://t.me/ftgmodulesbyfl1yd via gayporn
from .. import loader, utils
from asyncio import sleep

@loader.tds
class EternalOnlineMod(loader.Module):
    """Вечный онлайн."""
    strings = {'name': 'Eternal Online',
               'enable': 'Вечный онлайн включен.',
               'disable': 'Вечный онлайн выключен.'}

    async def client_ready(self, client, db):
        self.db = db

    async def onlinecmd(self, message):
        """Включить/Отключить вечный онлайн."""
        if not self.db.get("Eternal Online", "status"):
            self.db.set("Eternal Online", "status", True)
            await utils.answer(message, self.strings('enable', message))
            while self.db.get("Eternal Online", "status"):
                await message.client(__import__("telethon").functions.account.UpdateStatusRequest(offline=False))
                await sleep(60)

        else:
            self.db.set("Eternal Online", "status", False)
            await utils.answer(message, self.strings('disable', message))