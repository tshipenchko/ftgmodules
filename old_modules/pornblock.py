from .. import loader, utils
import functools
from telethon import events
import logging

logger = logging.getLogger(__name__)


@loader.tds
class PbanMod(loader.Module):
    """When you filter a text, it auto responds to it if a user triggers the word)"""
    strings = {"name": "PornBlock",
               "blocked": "{} was blocked",
               "unblocked": "{} was unblocked",
               "allunban": "All was unbanned",
               "list": "Blocked:\n{}",
               "message": "This channel can’t be displayed because it was used to spread pornographic content."}

    async def client_ready(self, client, db):
        self._db = db
        self._me = await client.get_me()
        if "PbanMod.watchout" not in str(client.list_event_handlers()):
            client.add_event_handler(
                functools.partial(self.watchout),
                events.NewMessage())

    async def pbancmd(self, message):
        """Adds a ban into the list"""
        args = utils.get_args(message)
        args = args if args else [str(message.chat_id)]
        filters = self._db.get("PBan", "banlist", [])
        filters.extend(args)
        filters = list(set(filters))
        self._db.set("PBan", "banlist", filters)
        await utils.answer(message, self.strings("blocked", message).format(filters))

    async def punbancmd(self, message):
        """Removes a ban from the list"""
        args = utils.get_args(message)
        args = args if args else [str(message.chat_id)]
        filters = list(set(self._db.get("PBan", "banlist", [])))
        for x in args:
            if x in filters:
                filters.remove(x)

        self._db.set("PBan", "banlist", filters)
        await utils.answer(message, self.strings("unblocked", message).format(filters))

    async def punbanallcmd(self, message):
        """Removes all bans from the list"""
        self._db.set("PBan", "banlist", [])
        await utils.answer(message, self.strings("allunban", message))

    async def pbanscmd(self, message):
        """Shows saved filters."""
        banlist = self._db.get("PBan", "banlist", [])
        text = "\n".join([f"<code>{x}</code>" for x in banlist])
        await utils.answer(message, self.strings("list", message).format(text))

    async def watchout(self, message):
        filters = self._db.get("PBan", "banlist", [])
        if str(message.chat_id) not in str(filters):
            return
        logger.info("Tried " + str(message.chat_id))
        if message.fwd_from:
            await message.delete()

        try:
            await message.edit("This channel can’t be displayed because it was used to spread pornographic content.", file=None)
        except Exception as e:  # иии так сойдёт
            logger.info("AH SHIT:")
            logger.info(e, exc_info=True)
